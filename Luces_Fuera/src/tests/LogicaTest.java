package tests;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import logica.Logica;

public class LogicaTest {
	Logica log=new Logica();

	@Test
	public void ganasteTest() {
		
		boolean[][] matriz = new boolean[3][3];
		log.limpiarMatriz(matriz);
		boolean ret=log.ganaste(matriz);
		Assert.assertEquals(true,ret);
		
		
	}
	@Test
	public void matrizEstadosAzarTest() {
		
		boolean[][] matriz = new boolean[3][3];
		log.matrizEstadosAzar(matriz);
		boolean ret=false;
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				ret= ret || matriz[i][j];
			}
		}
		assertTrue(ret);
		
		
	}
	@Test
	public void actualizarMatrizTest() {
		
		boolean[][] matriz = new boolean[3][3];
		log.limpiarMatriz(matriz);
		log.actualizarMatriz(matriz, 2,2);
		assertTrue(matriz[2][2]);
		assertTrue(matriz[2][1]);
		assertTrue(matriz[1][2]);
	}
	@Test
	public void comprobarRangoFueraTest()
	{
		boolean[][]matriz= new boolean[3][3];
		boolean ret= log.comprobarRango(matriz, 4, 0);
		assertFalse(ret);
		
	}
	@Test
	public void comprobarRangoDentroTest()
	{
		boolean[][]matriz= new boolean[3][3];
		boolean ret= log.comprobarRango(matriz, 2, 1);
		assertTrue(ret);
		
	}
}

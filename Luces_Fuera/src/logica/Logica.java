package logica;

public class Logica {
	
public Logica(){
		
	}
	
	public void matrizEstadosAzar(boolean[][] matriz) 
	{
		int cant =0;
		int lucesPrendidas = (matriz.length*matriz.length)/2;
		while(cant<lucesPrendidas) {
			int x = (int) (Math.random() * matriz.length);
			int y = (int) (Math.random() * matriz.length) ;
			
			if(matriz[x][y] == false ) {
				matriz[x][y]=true;
				cant++;
			}
			
		}
	}
	
	public void  limpiarMatriz(boolean[][] matriz) 
	{
		
		for (int fila = 0; fila <  matriz.length; fila++) {
			for (int col = 0; col <  matriz.length; col++) {
				 matriz[fila][col]=false;
			}
		}
		
	}
	
	public void actualizarMatriz(boolean[][] matriz, int x, int y) 
	{
		cambiarEsatado(matriz,x,y);
		cambiarEsatadoDer(matriz, x, y);
		cambiarEsatadoIzq(matriz, x, y);
		cambiarEsatadoSup(matriz, x, y);
		cambiarEsatadoInf(matriz, x, y);		
		
	}
	
	private void cambiarEsatado(boolean[][] matriz, int x, int y) 
	{
		if(matriz[x][y]==true)
			matriz[x][y]=false;
		else 
			matriz[x][y]=true;
	}
    private void cambiarEsatadoIzq(boolean[][] matriz, int x, int y) 
    {
    	if(comprobarRango(matriz,x-1,y)) {
    		if(matriz[x-1][y]==true)
    			matriz[x-1][y]=false;
    		else 
    			matriz[x-1][y]=true;
    	}
    	
	}
	private void cambiarEsatadoDer(boolean[][] matriz, int x, int y) 
	{
		if(comprobarRango(matriz,x+1,y)) {
			if(matriz[x+1][y]==true)
				matriz[x+1][y]=false;
			else 
				matriz[x+1][y]=true;
		}
	}
	private void cambiarEsatadoSup(boolean[][] matriz, int x, int y) 
	{
		if(comprobarRango(matriz,x,y-1)) {
			if(matriz[x][y-1]==true)
				matriz[x][y-1]=false;
			else 
				matriz[x][y-1]=true;
		}
	}
	private void cambiarEsatadoInf(boolean[][] matriz, int x, int y) 
	{
		
		if(comprobarRango(matriz,x,y+1)) {
			if(matriz[x][y+1]==true)
				matriz[x][y+1]=false;
			else 
				matriz[x][y+1]=true;
		}
	}
	
	public boolean comprobarRango(boolean[][] matriz,  int x,int y) {
		if(x>=0 && x<matriz.length && y>=0 && y<matriz[0].length )
			return true;
		else 
			return false;
	}
	
	public boolean ganaste(boolean[][] matriz) {
		boolean ret = false;
		for (int fila = 0; fila < matriz.length; fila++) {
			for (int columna = 0; columna < matriz.length; columna++) {
				ret = ret || matriz[fila][columna];
			}
			
		}
		if(ret == false)
			return true;
		else 
			return false;
	}
	

}

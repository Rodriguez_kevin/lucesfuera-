package ventana;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;


public class Menu extends JFrame 
{

	private JPanel contentPane;
	private Juego ventana;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		

		try {
            //here you can put the selected theme class name in JTattoo
            UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
 
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * creo el frame.
	 */
	public Menu() 
	{
		setTitle("                     LIGHT OUT");
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/imagenes/iconoMenu.png")));
		setBounds(100, 100, 400, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel tituloMenu = new JLabel("Light     ut");
		tituloMenu.setForeground(Color.ORANGE);
		tituloMenu.setFont(new Font("Gabriola", Font.BOLD | Font.ITALIC, 66));
		tituloMenu.setBounds(59, 82, 268, 73);
		contentPane.add(tituloMenu);
		
		botonesDePantalla();
		cargarImagenes();
		
	}
	
	public int tamanioElegido(int selec) 
	{
		
		if(selec == 0 || selec == 2) 
		{
			return 3;
		}
		else if (selec == 1) 
		{
			return 2;
		}
		else if (selec == 3) 
		{
			return 4;
		}
		else
		{
			return 5;
		}
	}
	
	public void cargarImagenes()
	{
		JLabel iconoTitulo = new JLabel("");
		iconoTitulo.setIcon(new ImageIcon(Menu.class.getResource("/imagenes/iconoMenu.png")));
		iconoTitulo.setBounds(219, 82, 48, 57);
		contentPane.add(iconoTitulo);
		
		
		JLabel fondoMenu = new JLabel("");
		fondoMenu.setIcon(new ImageIcon(Menu.class.getResource("/imagenes/FondoMenu.jpg")));
		fondoMenu.setBounds(0, 0, 395, 371);
		contentPane.add(fondoMenu);
	}
	
	private void botonesDePantalla() 
	{
		
		JComboBox opciones = new JComboBox();
		opciones.setForeground(Color.BLACK);
		opciones.setBackground(Color.ORANGE);
		opciones.setModel(new DefaultComboBoxModel(new String[] {"Niveles","2x2","3x3", "4x4", "5x5"}));
		opciones.setBounds(10, 11, 83, 20);
		contentPane.add(opciones);
		contentPane.setLayout(null);
		
		JButton btnJugar = new JButton("JUGAR");
		btnJugar.setForeground(Color.black);
		btnJugar.setBackground(Color.orange);
		btnJugar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana= new Juego(tamanioElegido(opciones.getSelectedIndex()));
				ventana.setVisible(true);
				ventana.setLocationRelativeTo(null);
				dispose();
				
			}
	    	@Override
	    	public void mouseEntered(MouseEvent arg0) {
	    		btnJugar.setBounds(82, 166, 236, 62);
	    		
	    	}
	    	
	    	@Override
	    	public void mouseExited(MouseEvent e) {
	    		btnJugar.setBounds(128, 166, 145, 62);
	    	}
		});
	    btnJugar.setBounds(128, 166, 145, 62);
		contentPane.add(btnJugar);
		
		JButton btnCreditos = new JButton("CREDITOS");
		btnCreditos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Icon icono = new ImageIcon(Menu.class.getResource("/imagenes/iconoMenu.png"));
				JOptionPane.showMessageDialog(null,"Desarrollado por:  Kevin Rodriguez & Ezequiel Diaz\n         PROGRAMACION III A�O 2019","Creditos", JOptionPane.INFORMATION_MESSAGE, icono );
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btnCreditos.setBounds(80, 254, 240, 62);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnCreditos.setBounds(128, 254, 145, 62);
			}
		});
		btnCreditos.setForeground(Color.black);
		btnCreditos.setBackground(Color.orange);
		btnCreditos.setBounds(128, 254, 145, 62);
		contentPane.add(btnCreditos);
	}
}


















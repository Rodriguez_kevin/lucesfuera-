package ventana;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;

import javax.swing.border.EmptyBorder;
import logica.Logica;
import ventana.Menu;

import javax.swing.*;



public class Juego extends JFrame {

	private JPanel contentPane;
	private Logica logica= new Logica();
	private int cantidadDeFilasDeBotones ;
    private int cantidadDecolumnaDeBotones;
    private boolean[][] matrizDeEstados;
    private JButton[][] grillaDeBotones;
    private JButton contador;
    private int turnos;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		try {
            //here you can put the selected theme class name in JTattoo
            UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
 
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Juego frame = new Juego(0);
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Juego(int t) 
	{
		cantidadDeFilasDeBotones=t ;
	    cantidadDecolumnaDeBotones=t;
	    matrizDeEstados = new boolean[cantidadDeFilasDeBotones ][cantidadDecolumnaDeBotones];
	    grillaDeBotones= new JButton[cantidadDeFilasDeBotones ][cantidadDecolumnaDeBotones];
	    turnos=0;
	    setTitle("                       LIGHT OUT");
	    setResizable(false);
	    setIconImage(Toolkit.getDefaultToolkit().getImage(Juego.class.getResource("/imagenes/iconoMenu.png")));
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100,100*cantidadDeFilasDeBotones+100 ,100*cantidadDecolumnaDeBotones+100 );
		contentPane = new JPanel();
		contentPane.setBackground(Color.darkGray);
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//INICIALIZAR CONTENIDOS DE PANTALLA
		logica.matrizEstadosAzar(matrizDeEstados);
		botonesDePantalla();
		crearGrillaDeBotones();
		actualizarGrilla();
		
		
		
		//FONDO
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Juego.class.getResource("/imagenes/backgraund2.jpg")));
		label.setBounds(1, 0, 600, 600);
		contentPane.add(label);
	}
	
	private void botonesDePantalla() 
	{
		int ancho=100*cantidadDeFilasDeBotones+100;
	    int alto =100*cantidadDecolumnaDeBotones+100;
	    
		JButton btnMenu = new JButton("MENU");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Menu menu= new Menu();
				menu.setVisible(true);
				menu.setLocationRelativeTo(null);	
				dispose();			}
		});
		btnMenu.setForeground(Color.black);
		btnMenu.setBackground(Color.ORANGE);
		btnMenu.setBounds(10, alto-62, 89, 23);
		contentPane.add(btnMenu);
		
		JButton btnReiniciar = new JButton("REINICIAR");
		btnReiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reiniciarGrilla();			}
		});
		btnReiniciar.setBackground(Color.ORANGE);
		btnReiniciar.setBounds(ancho-99,alto-62, 89, 23);
		contentPane.add(btnReiniciar);
		
		contador = new JButton("0");
		contador.setBounds((ancho-66)/2, alto-62, 66, 23);
		contentPane.add(contador);
	}
	
	private void crearGrillaDeBotones() 
	{
		for(int fila=0;fila<grillaDeBotones.length;fila++) 
		{
			for (int columna = 0; columna < grillaDeBotones[0].length; columna++) 
			{
				grillaDeBotones[fila][columna]= new JButton();
				grillaDeBotones[fila][columna].setBounds(50+fila*100, 20+100*columna, 100, 100);
				grillaDeBotones[fila][columna].setIcon(new ImageIcon(Juego.class.getResource("/imagenes/iconoFoco.png")));
				int coordenadaX = fila;
				int coordenadaY = columna;
				grillaDeBotones[fila][columna].addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e) 
					{
						logica.actualizarMatriz(matrizDeEstados, coordenadaX ,coordenadaY );
						actualizarGrilla();
						turnos++;
						contador.setText(""+turnos);
						if(logica.ganaste(matrizDeEstados)) {
							String msj= "                !!!FELICITACIONES!!!   \n Has logrado apagar todas las luces en "+turnos+" intentos";
							Icon icono =  new ImageIcon(Juego.class.getResource("/imagenes/iconFiesta.png"));
							JOptionPane.showMessageDialog(null,msj,"GANASTE!!!", JOptionPane.INFORMATION_MESSAGE,icono);
						}
					}
				});
				JButton boton= grillaDeBotones[fila][columna];
				contentPane.add(boton);
			}
		}
	}

	private void actualizarGrilla() 
	{
		for(int fila=0;fila<grillaDeBotones.length;fila++) 
		{
			for (int columna = 0; columna < grillaDeBotones[0].length; columna++) 
			{
				JButton boton = grillaDeBotones[fila][columna];
				actualizarBoton(boton, fila, columna);
			}
		}
	}
	
	private void actualizarBoton(JButton boton, int i, int j) 
	{
		if(matrizDeEstados[i][j]== true)
			boton.setBackground(Color.yellow);
		else 
			boton.setBackground(Color.gray);
	}
	
	private void reiniciarGrilla() 
	{
		turnos=0;
		contador.setText(""+turnos);
		logica.limpiarMatriz(matrizDeEstados);
		logica.matrizEstadosAzar(matrizDeEstados);
		actualizarGrilla();
	}
}
